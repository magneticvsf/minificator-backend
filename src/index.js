const express = require('express');
const requireDir = require('require-dir');

const urlsModule = requireDir('./controllers/urls');
const clicksModule = requireDir('./controllers/clicks');

const routes = express.Router({mergeParams: true});

function nestedRoutes(path, configure) {
    const router = express.Router({mergeParams: true});
    this.use(path, router);
    configure(router);
    return router;
}

express.application.prefix = nestedRoutes;
express.Router.prefix = nestedRoutes;

routes.get('/', (req, res) => {
    res.send('Salom!');
});

routes.prefix('/clicks/', clicks => {
    clicks.get('/count', clicksModule.clicks.getCount);
})
routes.prefix('/urls', urls => {
    urls.get('/', urlsModule.urls.getAll);
})
routes.prefix('/url', url => {
    url.prefix('/clicks', clicks => {
        clicks.get('/:link', clicksModule.clicks.getAll);
    });
    url.post('/', urlsModule.urls.add);
})

routes.get('/:link', urlsModule.urls.link);

module.exports = routes;




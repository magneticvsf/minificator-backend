const mysql = require('mysql');

const config = require('../../config.json');

function disconnect() {
    let connection = mysql.createConnection({
        host: config.db.hostname,
        database: config.db.database,
        user: config.db.username,
        password: config.db.password
    });
    connection.connect(err => {
        if(err) {
            console.error('Connection failed!');
            setTimeout(disconnect, 2000);
        }
        console.log('Connected!');
    });
    connection.on('error', err => {
        if(err.code === 'PROTOCOL_CONNECTION_LOST') {
          disconnect();
        }
      });
    module.exports = connection;
}
disconnect();
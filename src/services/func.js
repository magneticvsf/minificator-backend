module.exports.generate = (limit = 6) => {
    const map = `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`;
    const length = map.length;
    let res = ``;
    for(let i = 0; i < limit; i++){
        let rand = Math.floor(Math.random() * 999) % length;
        res += map[rand];
    }
    return res;
}
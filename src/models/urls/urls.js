const func = require('../../services/func');

module.exports.getAll = async (params, cb) => {
    let db = require('../../services/db');
    let LIMIT = '';
    let result = {};
    if(params.limit && params.page) {
        let offset = params.limit * (params.page - 1);
        LIMIT = ` LIMIT ${offset}, ${params.limit}`;
    }
    await db.query(`SELECT u.*,
                (SELECT COUNT(c.id) FROM clicks c WHERE c.url_id = u.id) AS clicks
            FROM urls u ORDER BY id DESC ${LIMIT}`, (err, res) => {
        if(err) return cb(err);
        result.items = res;
    });
    await db.query(`SELECT COUNT(id) AS count FROM urls`, (err, res) => {
        if(err) return cb(err);
        result.count = res[0].count;
        return cb(null, result);
    });
}

module.exports.get = (params, cb) => {
    let db = require('../../services/db');
    db.query(`SELECT * FROM urls WHERE min_url='${params.min_url}'`, (err, res) => {
        if(err) return false;
        return cb(err, res[0]);
    });
}

module.exports.add = async (params, cb) => {
    let db = require('../../services/db');
    const time = new Date();
    const minUrl = await func.generate();
    await db.query(`INSERT INTO urls SET
                    url='${params.url}',
                    min_url='${minUrl}',
                    fallow_date='${params.fallowDate}'`,
        (err, res) => {
        if(err) return cb(err);
        return cb(null, true);
    });
}
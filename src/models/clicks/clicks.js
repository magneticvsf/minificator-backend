
module.exports.getAll = (params, cb) => {
    let db = require('../../services/db');
    db.query(`SELECT * FROM clicks WHERE url_id='${params.id}'`, (err, res) => {
        if(err) return false;
        return cb(err, res);
    });
}
module.exports.getCount = (cb) => {
    let db = require('../../services/db');
    db.query(`SELECT COUNT(id) AS count FROM clicks`, (err, res) => {
        if(err) return false;
        return cb(err, res[0]);
    });
}
module.exports.add = async (params, cb) => {
    let db = require('../../services/db');
    const time = new Date();
    await db.query(`INSERT INTO clicks SET
                    url_id='${params.id}',
                    platform='${params.platform}',
                    browser='${params.browser}',
                    os='${params.os}',
                    referer_url='${params.referer_url}'`, (err, res) => {
        if(err) return cb(err);
        return cb(null, true);
    });
}
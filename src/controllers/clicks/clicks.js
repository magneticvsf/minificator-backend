const urlsModel = require('../../models/urls/urls');
const clicksModel = require('../../models/clicks/clicks');

module.exports.getAll = (req, res) => {
    let params = {
        min_url: req.params.link
    };
    urlsModel.get(params, (err, url) => {
        params.id = url.id;
        clicksModel.getAll(params, (err, urls) => {
            if(err) res.json(err);
            res.json(urls);
            res.end();
        });
    });
};
module.exports.getCount = (req, res) => {
    clicksModel.getCount((err, count) => {
        if(err) return 0;
        res.send(count);
    });
};
const urlsModel = require('../../models/urls/urls');
const clicksModel = require('../../models/clicks/clicks');
const useragent = require('express-useragent');

module.exports.getAll = (req, res) => {
    let params = {
        limit: req.query.limit,
        page: req.query.page
    };
    urlsModel.getAll(params, (err, urls) => {
        res.json(urls);
    });
};

module.exports.add = (req, res) => {
    let params = {
        url: req.body.url,
        fallowDate: req.body.fallow_date
    };
    urlsModel.add(params, (err, ans) => {
        res.send(ans);
    });

}

module.exports.link = (req, res) => {
    if(req.params.link == 'favicon.ico') return;
    console.log(req.params);
    let ua = useragent.parse(req.headers['user-agent']);
    let params = {
        min_url: req.params.link,
        referer_url: req.headers.referer,
        browser: ua.browser,
        platform: ua.platform,
        os: ua.os
    }
    urlsModel.get(params, (err, url) => {
        if(err) {
            res.send(err);
            res.end();
        }
        params.id = url.id;
        clicksModel.add(params, (err, result) => { console.log(result) });
        res.redirect(url.url);
    });
}
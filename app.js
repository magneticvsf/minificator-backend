const express = require('express');

const config = require('./config.json');

const app = express();
require('./use')(app);

app.listen(config.app.port,
    console.log(`${config.app.name} service app listening on port ${config.app.port}`));